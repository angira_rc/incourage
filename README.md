# Incourage Sales API
This is the documentation showing how to initialize and run this API.

## The Setup
1. Install dependencies
   - Note: The project was set up using the <b>yarn install</b> command.
   - <code>yarn install</code>
   - <code>npm install</code>
2. Ensure your mongodb instance is up and running
3. Create your env file
    - Below is a sample of the required .env file<br/>
    <code>
      MONGODB_URI=mongodb://localhost:27017/incourage
      JWT_SECRET=79b7225e-9a58-4498-8b04-bf42cd171cec
      COMPANY_NAME=Incourage Inc.
      EMAIL_ADDRESS=destany.kshlerin@ethereal.email
      EMAIL_PASSWORD=c3NE4eMdhkTjhznFND
      EMAIL_HOST=smtp.ethereal.email
      EMAIL_PORT=587
    </code>

## Initializing the System
- Send a POST request to the <b>/auth/initialize</b> endpoint
- This will initialize the root user username: <b>root</b>, password: <b>pass@inc0urage</b> as well as other system parameters such as commission percentage.
- This can only be run once.
- Great! You're good to go

## Running the API
### Auth Operations
#### Agent Sign Up
- Endpoint: <b>/auth/sign-up</b>
- Method: POST
- Sample Request Body<br/>
<code>
  {
    "name": "Reinhardt Angira",
    "username": "reiny",
    "password": "pass",
    "email": "reinhardtangira@mail.com"
  }
</code>
- Response:<br/>
<code>
  {
    "name": "Reinhardt Angira",
    "username": "reiny",
    "email": "reinhardtangira@mail.com",
    "type": "agent",
    "_id": "65f8bc00f254554dd6299bd7",
    "date": "2024-03-18T22:11:12.607Z",
    "__v": 0
  }
</code>

#### Agent Login
- Endpoint: <b>/auth/login</b><br/>
- Method: POST
- Sample Request Body<br/>
  <code>
    {
      "username": "reiny",
      "password": "pass",
    }
  </code>
- Response:<br/>
  <code>
    {
      "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY1ZjhiYzAwZjI1NDU1NGRkNjI5OWJkNyIsInVzZXJuYW1lIjoicmVpbnkiLCJpYXQiOjE3MTA4MjQ4MzUsImV4cCI6MTcxMDgyNTQzNX0.d_s7JbdV3SNzTcreKhhKajlmw1b4198oNTsemICwI5s"
    }
  </code>

### User Operations
#### List Users
- Endpoint: <b>/users</b>
- Method: GET
- Authentication: Bearer Token <b>(Admin Only)</b>
- Query Parameters
- Search String (Optional)
  - Filters users with names matching the search string.
  - Sample: ?string={searchString}
- Response:<br/>
<code>
  [
    {
      "_id": "65f83cbe593be47f79ff9838",
      "name": "Root User",
      "username": "root",
      "email": "root@incourage.com",
      "type": "admin",
      "date": "2024-03-18T13:08:14.690Z",
      "__v": 0
    },
    {
      "_id": "65f845261456d9f69dd64a2e",
      "name": "Reinhardt Angira",
      "username": "reiny",
      "email": "reinhardtangira@mail.com",
      "type": "agent",
      "date": "2024-03-18T13:44:06.827Z",
      "__v": 0
    }
  ]
</code>

#### Show Single User
- Endpoint: <b>/users/{ user_id }</b>
- Method: GET
- Authentication: Bearer Token <b>(Admin Only)</b>
- Response:<br/>
<code>
  {
    "_id": "65f845261456d9f69dd64a2e",
    "name": "Reinhardt Angira",
    "username": "reiny",
    "email": "reinhardtangira@mail.com",
    "type": "agent",
    "date": "2024-03-18T13:44:06.827Z",
    "__v": 0
  }
</code>

#### Add New User
- Endpoint: <b>/users</b>
- Method: POST
- Authentication: Bearer Token <b>(Admin Only)</b>
- Sample Request Body:<br/>
<code>
  {
    "name": "Reinhardt Angira",
    "username": "reiny",
    "password": "pass",
    "type": "agent",
    "email": "reinhardtangira@mail.com"
  }
</code>
- Response:<br/>
<code>
{
  "name": "Reinhardt Angira",
  "username": "reiny",
  "email": "reinhardtangira@mail.com",
  "type": "agent",
  "_id": "65f8bc00f254554dd6299bd7",
  "date": "2024-03-18T22:11:12.607Z",
  "__v": 0
}
</code>

#### Edit User Details
- Endpoint: <b>/users/{ user_id }</b>
- Method: PUT
- Authentication: Bearer Token <b>(Admin Only)</b>
- Sample Request Body:<br/>
<code>
  {
    "name": "Reinhardt Angira",
    "username": "reiny",
    "password": "pass",
    "email": "reinhardtangira@mail.com"
  }
</code>
- Note: Root user's username cannot be edited
- Response:<br/>
<code>
{
  "name": "Reinhardt Angira",
  "username": "reiny",
  "email": "reinhardtangira@mail.com",
  "type": "agent",
  "_id": "65f8bc00f254554dd6299bd7",
  "date": "2024-03-18T22:11:12.607Z",
  "__v": 0
}
</code>

#### Delete User: <b>/users/{ user_id }</b>
- Method: DELETE
- Authentication: Bearer Token <b>(Admin Only)</b>
- Note: Root user cannot be deleted
- Response:<br/>
<code>
{
  "name": "Reinhardt Angira",
  "username": "reiny",
  "email": "reinhardtangira@mail.com",
  "type": "agent",
  "_id": "65f8bc00f254554dd6299bd7",
  "date": "2024-03-18T22:11:12.607Z",
  "__v": 0
}
</code>

### Get Statement
- Enables the logged in user to request their statement for a specified period of time.
- An Email with an attached PDF document will be send to the user's registered email.
- Endpoint: <b>/getStatement</b>
- Method: GET
- Authentication: Bearer Token
- Query Parameters
- From (Required)
  - Filters legder entries recorded after the specified date.
  - Sample: ?from=2024-01-17
- To (Required)
  - Filters legder entries recorded before the specified date.
  - Sample: ?to=2024-01-19
- Response:<br/>
<code>
  {
    "status": "OK"
  }
</code>

### Product Operations
#### List Products
- Endpoint: <b>/products</b>
- Method: GET
- Authentication: Bearer Token
- Query Parameters
- Search String (Optional)
  - Filters products with names matching the search string.
  - Sample: ?string={searchString}
- Category (Optional)
  - Filters products with the specified category.
  - Sample: ?category={category}
- Response:<br/>
<code>
  [
    {
      "_id": "65f46120cbc128219be30c0b",
      "name": "IPhone 15 Plus",
      "category": "Mobile Phones",
      "price": 190000,
      "date": "2024-03-15T14:54:24.621Z",
      "__v": 0
    },
    {
      "_id": "65f8442a2b1fa2c401c3e04a",
      "name": "Samsung Galaxy S24 Ultra",
      "category": "Mobile Phones",
      "price": 205000,
      "date": "2024-03-18T13:39:54.435Z",
      "__v": 0
    }
  ]
</code>

#### Show Single Product
- Endpoint: <b>/products/{ product_id }</b>
- Method: GET
- Authentication: Bearer Token
- Response:<br/>
<code>
  {
    "_id": "65f8442a2b1fa2c401c3e04a",
    "name": "Samsung Galaxy S24 Ultra",
    "category": "Mobile Phones",
    "price": 205000,
    "date": "2024-03-18T13:39:54.435Z",
    "__v": 0
  }
</code>

#### Add New Product
- Endpoint: <b>/products</b>
- Method: POST
- Authentication: Bearer Token <b>(Admin Only)</b>
- Sample Request Body:<br/>
<code>
  {
    "name": "Samsung Galaxy S24 Ultra",
    "category": "Mobile Phones",
    "price": 204000
  }
</code>
- Response:<br/>
<code>
{
  "name": "Samsung Galaxy S24 Ultra",
  "category": "Mobile Phones",
  "price": 204000,
  "_id": "65f8442a2b1fa2c401c3e04a",
  "date": "2024-03-18T13:39:54.435Z",
  "__v": 0
}
</code>

#### Edit Product Details
- Endpoint: <b>/products/{ product_id }</b>
- Method: PUT
- Authentication: Bearer Token <b>(Admin Only)</b>
- Sample Request Body:<br/>
<code>
  {
    "name": "Samsung Galaxy S24 Ultra",
    "category": "Mobile Phones",
    "price": 205000
  }
</code>
- Response:<br/>
<code>
  {
    "_id": "65f8442a2b1fa2c401c3e04a",
    "name": "Samsung Galaxy S24 Ultra",
    "category": "Mobile Phones",
    "price": 205000,
    "date": "2024-03-18T13:39:54.435Z",
    "__v": 0
  }
</code>

#### Delete Product
- Endpoint: <b>/products/{ product_id }</b>
- Method: DELETE
- Authentication: Bearer Token <b>(Admin Only)</b>
- Response:<br/>
<code>
  {
    "_id": "65f8442a2b1fa2c401c3e04a",
    "name": "Samsung Galaxy S24 Ultra",
    "category": "Mobile Phones",
    "price": 205000,
    "date": "2024-03-18T13:39:54.435Z",
    "__v": 0
  }
</code>

### Transactions
#### List Transactions
- Endpoint: <b>/transactions</b>
- Method: GET
- Authentication: Bearer Token <b>(Admin Only)</b>
- Query Parameters
- From (Required)
  - Filters transactions recorded after the specified date.
  - Sample: ?from=2024-01-17
- To (Required)
  - Filters transactions recorded before the specified date.
  - Sample: ?to=2024-01-19
- Product (Optional)
  - Filters transactions for a specified product id.
  - Sample: ?product={ product_id }
- User (Optional)
  - Filters transactions recorded by a specified user.
  - Sample: ?user={ user_id }
- Response:<br/>
<code>
  [
    {
      "_id": "65f860431688970f9bb2a135",
      "agent": {
        "_id": "65f845261456d9f69dd64a2e",
        "name": "Reinhardt Angira",
        "username": "reiny",
        "email": "reinhardtangira@mail.com",
        "type": "admin",
        "date": "2024-03-18T13:44:06.827Z",
        "__v": 0
      },
      "product": {
        "_id": "65f8442a2b1fa2c401c3e04a",
        "name": "Samsung Galaxy S24 Ultra",
        "category": "Mobile Phones",
        "price": 205000,
        "date": "2024-03-18T13:39:54.435Z",
        "__v": 0
      },
      "amount": 205000,
      "quantity": 1,
      "legder": "65f860431688970f9bb2a133",
      "date": "2024-03-18T15:39:47.574Z",
      "__v": 0
    }
  ]
</code>

#### List Transactions By Logged In User
- Endpoint: <b>/transactions/mine</b>
- Method: GET
- Authentication: Bearer Token
- Query Parameters
- From (Required)
  - Filters transactions recorded after the specified date.
  - Sample: ?from=2024-01-17
- To (Required)
  - Filters transactions recorded before the specified date.
  - Sample: ?to=2024-01-19
- Product (Optional)
  - Filters transactions for a specified product id.
  - Sample: ?product={ product_id }
- Response:<br/>
<code>
  [
    {
      "_id": "65f860431688970f9bb2a135",
      "agent": {
        "_id": "65f845261456d9f69dd64a2e",
        "name": "Reinhardt Angira",
        "username": "reiny",
        "email": "reinhardtangira@mail.com",
        "type": "admin",
        "date": "2024-03-18T13:44:06.827Z",
        "__v": 0
      },
      "product": {
        "_id": "65f8442a2b1fa2c401c3e04a",
        "name": "Samsung Galaxy S24 Ultra",
        "category": "Mobile Phones",
        "price": 205000,
        "date": "2024-03-18T13:39:54.435Z",
        "__v": 0
      },
      "amount": 205000,
      "quantity": 1,
      "legder": "65f860431688970f9bb2a133",
      "date": "2024-03-18T15:39:47.574Z",
      "__v": 0
    }
  ]
</code>

#### Show Single Transaction
- Endpoint: <b>/transactions/{ transaction_id }</b>
- Method: GET
- Authentication: Bearer Token <b>(Admin Only)</b>
- Response:<br/>
<code>
  {
    "_id": "65f860431688970f9bb2a135",
    "agent": {
      "_id": "65f845261456d9f69dd64a2e",
      "name": "Reinhardt Angira",
      "username": "reiny",
      "email": "reinhardtangira@mail.com",
      "type": "admin",
      "date": "2024-03-18T13:44:06.827Z",
      "__v": 0
    },
    "product": {
      "_id": "65f8442a2b1fa2c401c3e04a",
      "name": "Samsung Galaxy S24 Ultra",
      "category": "Mobile Phones",
      "price": 205000,
      "date": "2024-03-18T13:39:54.435Z",
      "__v": 0
    },
    "amount": 205000,
    "quantity": 1,
    "legder": "65f860431688970f9bb2a133",
    "date": "2024-03-18T15:39:47.574Z",
    "__v": 0
  }
</code>

#### Record New Transaction
- Endpoint: <b>/transactions</b>
- Method: POST
- Authentication: Bearer Token
- Sample Request Body:<br/>
<code>
  {
    "product": "65f8442a2b1fa2c401c3e04a",
    "amount": 205000,
    "quantity": 1
  }
</code>
- Response:<br/>
<code>
  {
    "agent": "65f8bc00f254554dd6299bd7",
    "product": "65f8442a2b1fa2c401c3e04a",
    "amount": 205000,
    "quantity": 1,
    "legder": "65f9193be99d3b1061a82ff9",
    "_id": "65f9193be99d3b1061a82ffb",
    "date": "2024-03-19T04:48:59.954Z",
    "__v": 0
  }
</code>

### Legder Operations
#### Show legder for a specified user
- Endpoint: <b>/legder{ user_id }</b>
- Method: GET
- Authentication: Bearer Token <b>(Admin Only)</b>
- Query Parameters
- From (Required)
  - Filters legder entries recorded after the specified date.
  - Sample: ?from=2024-01-17
- To (Required)
  - Filters legder entries recorded before the specified date.
  - Sample: ?to=2024-01-19
- Response:<br/>
<code>
  {
    "totalSales": 410000,
    "legder": [
      {
        "_id": "65f8bd13c3f92d932fbbe675",
        "user": "65f8bc00f254554dd6299bd7",
        "description": "Commission earned from sale of Samsung Galaxy S24 Ultra",
        "debit": 0,
        "credit": 10250,
        "balance": 10250,
        "value": 205000,
        "date": "2024-03-18T22:15:47.056Z",
        "__v": 0
      },
      {
        "_id": "65f9166d3cdebc6a056a1ea2",
        "user": "65f8bc00f254554dd6299bd7",
        "description": "Commission earned from sale of Samsung Galaxy S24 Ultra",
        "debit": 0,
        "credit": 10250,
        "balance": 20500,
        "value": 205000,
        "date": "2024-03-19T04:37:01.479Z",
        "__v": 0
      }
    ]
  }
</code>

#### Show legder for logged in user
- Endpoint: <b>/legder</b>
- Method: GET
- Authentication: Bearer Token
- Query Parameters
- From (Required)
  - Filters transactions recorded after the specified date.
  - Sample: ?from=2024-01-17
- To (Required)
  - Filters transactions recorded before the specified date.
  - Sample: ?to=2024-01-19
- Product (Optional)
  - Filters transactions for a specified product id.
  - Sample: ?product={ product_id }
- Response:<br/>
<code>
  {
    "totalSales": 410000,
    "legder": [
      {
        "_id": "65f8bd13c3f92d932fbbe675",
        "user": "65f8bc00f254554dd6299bd7",
        "description": "Commission earned from sale of Samsung Galaxy S24 Ultra",
        "debit": 0,
        "credit": 10250,
        "balance": 10250,
        "value": 205000,
        "date": "2024-03-18T22:15:47.056Z",
        "__v": 0
      },
      {
        "_id": "65f9166d3cdebc6a056a1ea2",
        "user": "65f8bc00f254554dd6299bd7",
        "description": "Commission earned from sale of Samsung Galaxy S24 Ultra",
        "debit": 0,
        "credit": 10250,
        "balance": 20500,
        "value": 205000,
        "date": "2024-03-19T04:37:01.479Z",
        "__v": 0
      }
    ]
  }
</code>

#### Pay Commission to Agent
- Endpoint: <b>/legder/{ user_id }/pay</b>
- Method: POST
- Authentication: Bearer Token
- Sample Request Body:<br/>
<code>
  {
    "amount": 10000
  }
</code>
- Response:<br/>
<code>
  {
    "_id": "65f8bd13c3f92d932fbbe675",
    "user": "65f8bc00f254554dd6299bd7",
    "description": "Commission paid to Reinhardt Angira",
    "debit": 10000,
    "credit": 0,
    "balance": 250,
    "value": 0,
    "date": "2024-03-18T22:15:47.056Z",
    "__v": 0
  }
</code>