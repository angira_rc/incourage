import { Controller, Get, UseGuards, Query, Request, Param, Post, Body, ValidationPipe } from '@nestjs/common';

import { ReqWithUser } from '../modules/interfaces';
import { AdminGuard, AuthGuard } from '../auth/auth.guard';
import { MongoOIDPipe, ParseDatePipe } from '../modules/pipes';

import { LegderService } from './legder.service';
import { PayCommissionSchema } from './dto';

@Controller('legder')
export class LegderController {  
    constructor(private readonly service: LegderService) {}
    
    @Get()
    @UseGuards(AuthGuard)
    getMyLegder(@Query('from', ParseDatePipe) from: Date, @Query('to', ParseDatePipe) to: Date, @Request() request: ReqWithUser) {
        try {
            return this.service.getLegder(request.user._id, from, to)
        } catch (e) {
            throw e
        }  
    }
    
    @Get(':id')
    @UseGuards(AdminGuard)
    getLegder(@Param('id', MongoOIDPipe) id: string, @Query('from', ParseDatePipe) from: Date, @Query('to', ParseDatePipe) to: Date) {
        try {
            return this.service.getLegder(id, from, to)
        } catch (e) {
            throw e
        }  
    }

    // Pay Commission
    @Post(':id/pay')
    @UseGuards(AdminGuard)
    payCommission(@Param('id', MongoOIDPipe) id: string, @Body(new ValidationPipe()) body: PayCommissionSchema, @Request() request?: ReqWithUser) {
        try {
            return this.service.payCommission(body, id, request.user)
        } catch (e) {
            throw e
        }  
    }
}
