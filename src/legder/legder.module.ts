import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { LegderSchema, UserSchema } from '../models/user';

import { LegderService } from './legder.service';
import { LegderController } from './legder.controller';

@Module({
  imports: [MongooseModule.forFeature([
    { name: 'Legder', schema: LegderSchema },
    { name: 'User', schema: UserSchema },
  ])],
  controllers: [LegderController],
  providers: [LegderService]
})
export class LegderModule {}