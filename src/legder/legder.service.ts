import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Legder, User } from '../models/user';
import { PayCommissionSchema } from './dto';
import { WasNotFound } from '../modules/exceptions';

@Injectable()
export class LegderService {
    constructor(
        @InjectModel(Legder.name) private legderModel: Model<Legder>,
        @InjectModel(User.name) private userModel: Model<User>) {}

    async getLegder(user: string, from: Date, to: Date): Promise<{ totalSales: number, legder: Legder[] }> {
        let ops: any = { user }
        
        if (from || to) {
            ops.date = {}
            if (from)
                ops.date.$gte = from

            if (to)
                ops.date.$lte = to
        }

        let legder = await this.legderModel.find(ops).lean()
        let totals = legder.reduce((total, item) => total + item.value, 0)
            
        return { totalSales: totals, legder }
    }

    async payCommission(body: PayCommissionSchema, id: string, user: User): Promise<Legder> {
        let agent = await this.userModel.findById(id)
        if (!agent)
            throw new WasNotFound('agent')

        let latest = await this.legderModel.find({ user: id }).sort({ date: -1 }).limit(1)
        let balance = latest.length > 0 ? latest[0].balance : 0
        
        if (balance < body.amount)
            throw new HttpException('Insufficient balance', HttpStatus.BAD_REQUEST)

        let legder = new this.legderModel({
            user: id,
            description: `Commission paid to ${agent.name}`,
            credit: 0,
            debit: body.amount,
            value: body.amount,
            balance: balance - body.amount
        })

        legder = await legder.save()

        return legder
    }
}