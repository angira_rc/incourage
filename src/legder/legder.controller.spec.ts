import * as moment from 'moment';
import { Test } from '@nestjs/testing';
import { UserSchema } from '../models/user';
import { LegderService } from './legder.service';
import { LegderController } from './legder.controller';
import { testBootstrap } from '../modules/bootstrap';

describe('LegderController', () => {
    let ledgerController: LegderController;
    let ledgerService: LegderService;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule(testBootstrap).compile();

        ledgerService = moduleRef.get<LegderService>(LegderService);
        ledgerController = moduleRef.get<LegderController>(LegderController);
    });

    describe('getLegder', () => {
        it('should return an object showing ledger entries for the specified period ', async () => {
            const result = {
                totalSales: 410000,
                legder: [
                    {
                        _id: "400",
                        user: "65",
                        description: "Commission earned from sale of Samsung Galaxy S24 Ultra",
                        debit: 0,
                        credit: 10250,
                        balance: 10250,
                        value: 205000,
                        date: new Date()
                    }
                ]
            }

            jest.spyOn(ledgerService, 'getLegder').mockImplementation(() => Promise.resolve(result));
            let res = await ledgerController.getLegder("0", moment().startOf('month').toDate(), moment().endOf('month').toDate())

            expect(res).toBe(result);
        });
    });

    describe('payCommission', () => {
        it('should deduct the commission from the user\'s balance', async () => {
            const result = {
                _id: "400",
                user: "50",
                description: "Commission paid to Reinhardt Angira",
                debit: 10000,
                credit: 0,
                balance: 10250,
                value: 205000,
                date: new Date()
            }

            jest.spyOn(ledgerService, 'payCommission').mockImplementation(() => Promise.resolve(result));
            let res = await ledgerController.payCommission("0", {
                amount: 10000
            })

            expect(res).toBe(result);
        });
    });
});