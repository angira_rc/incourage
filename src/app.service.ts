import moment from 'moment';
import { Model } from 'mongoose';
import { Cron } from '@nestjs/schedule';
import { Legder, User } from './models/user';
import { InjectModel } from '@nestjs/mongoose';
import { Injectable, Logger } from '@nestjs/common';

import { PdfService } from './pdf/pdf.service';
import { stringifyDate } from './modules/helpers';
import { EmailService } from './email/email.service';

@Injectable()
export class AppService {
    private readonly logger = new Logger(AppService.name);

    constructor(
        @InjectModel(User.name) private userModel: Model<User>, 
        @InjectModel(Legder.name) private legderModel: Model<Legder>, 
        private readonly mailService: EmailService,
        private readonly pdfService: PdfService) { }

    getHello(): string {
        return 'Hello World!';
    }

    async sendStatement(from: Date, to: Date, user: User) {
        let ops: any = { user: user._id }

        if (from || to) {
            ops.date = {}
            if (from)
                ops.date.$gte = from

            if (to)
                ops.date.$lte = to
        }

        let legder: Legder[] = await this.legderModel.find(ops).sort({ date: -1 }).lean()
        let total = 0
        let balance = legder.length > 0 ? legder[0].balance : 0
        
        let transactions: any[] = legder.map(item => {
            total += item.value
            return {
                ...item,
                date: stringifyDate(item.date, true),
            }
        })

        let [filename, path] = await this.pdfService.generate({
            user,
            from: stringifyDate(from),
            to: stringifyDate(to),
            total,
            balance,
            transactions
        })
        
        await this.mailService.sendEmail({
            to: user.email,
            subject: `Statement Between ${stringifyDate(from, true)} and ${stringifyDate(to, true)}`,
            content: `<h1>Your statement between ${stringifyDate(from, true)} and ${stringifyDate(to, true)}</h1>`,
            attachments: [
                {
                    filename,
                    path
                }
            ]
        })

        await this.pdfService.clearTemp(path)

        return { status: "OK" }
    }

    @Cron('0 8 15 * *')
    async sendStatements() {
        let users: User[] = await this.userModel.find({ type: 'agent' })
        let from: Date = moment().startOf('month').toDate()

        for await (let user of users)
            await this.sendStatement(from, new Date(), user)
        
        this.logger.debug('Statement Sent');
    }
}
