import { PartialType } from "@nestjs/mapped-types";
import { MinLength, IsEnum } from "class-validator";

export class NewUserSchema {
    @MinLength(3)
    name: string;
    
    @MinLength(4)
    username: string;
    
    @MinLength(6)
    email: string;
    
    @MinLength(6)
    password: string;
    
    @IsEnum(['admin', 'agent'])
    type: string;
}

export class UpdateUserSchema extends PartialType(NewUserSchema) {}