import { MongooseModule } from '@nestjs/mongoose';

import { Test } from '@nestjs/testing';
import { UserSchema } from '../models/user';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { testBootstrap } from '../modules/bootstrap';

describe('UsersController', () => {
    let usersController: UsersController;
    let usersService: UsersService;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule(testBootstrap).compile();

        usersService = moduleRef.get<UsersService>(UsersService);
        usersController = moduleRef.get<UsersController>(UsersController);
    });

    describe('getUsers', () => {
        it('should return an array of users', async () => {
            const result = [
                {
                    _id: "0",
                    username: 'robin',
                    name: 'Robin Angira',
                    email: 'rangira@gmail.com',
                    type: 'admin',
                    date: new Date(),
                }
            ];
            jest.spyOn(usersService, 'getUsers').mockImplementation(() => Promise.resolve(result));
            let res = await usersController.getUsers()
            
            expect(res).toBe(result);
        });
        
        it('should return an array of users matching "test"', async () => {
            const result = ['test'];
            jest.spyOn(usersService, 'getUsers').mockImplementation(() => Promise.resolve(result));
            let res = await usersController.getUsers('test')
            
            expect(res).toBe(result);
        });
    });

    describe('getUser', () => {
        it('should return a single user', async () => {
            const result = {
                _id: "0",
                username: 'robin',
                name: 'Robin Angira',
                email: 'rangira@gmail.com',
                type: 'admin',
                password: 'password',
                date: new Date(),
            }

            jest.spyOn(usersService, 'getUser').mockImplementation(() => Promise.resolve(result));
            let res = await usersController.getUser(result._id)
            
            expect(res).toBe(result);
        });
    });

    describe('newUser', () => {
        it('should add a new user', async () => {
            const result = {
                _id: "0",
                username: 'robin',
                name: 'Robin Angira',
                email: 'rangira@gmail.com',
                type: 'admin',
                password: 'password',
                date: new Date(),
            }

            jest.spyOn(usersService, 'newUser').mockImplementation(() => Promise.resolve(result));
            let res = await usersController.newUser({
                username: 'robin',
                name: 'Robin Angira',
                email: 'rangira@gmail.com',
                password: 'password',
                type: 'admin'
            })
            
            expect(res).toBe(result);
        });
    });

    describe('updateUser', () => {
        it('should update the user\'s details', async () => {
            const result = {
                _id: "0",
                username: 'robin',
                name: 'Robin Angira',
                email: 'rangira@gmail.com',
                type: 'admin',
                password: 'password1',
                date: new Date(),
            }

            jest.spyOn(usersService, 'updateUser').mockImplementation(() => Promise.resolve(result));
            let res = await usersController.updateUser("0", {
                username: 'robin',
                name: 'Robin Angira',
                email: 'rangira@gmail.com',
                password: 'password1',
                type: 'admin'
            })
            
            expect(res).toBe(result);
        });
    });

    describe('deleteUser', () => {
        it('should delete new user', async () => {
            const result = {
                _id: "0",
                username: 'robin',
                name: 'Robin Angira',
                email: 'rangira@gmail.com',
                type: 'admin',
                password: 'password1',
                date: new Date(),
            }

            jest.spyOn(usersService, 'removeUser').mockImplementation(() => Promise.resolve(result));
            let res = await usersController.deleteUser("0")
            
            expect(res).toBe(result);
        });
    });
});