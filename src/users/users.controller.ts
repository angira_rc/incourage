import { Body, Controller, Delete, Get, Param, Post, Put, Query, ValidationPipe, UseGuards } from '@nestjs/common';
import { NewUserSchema, UpdateUserSchema } from './dto';
import { UsersService } from './users.service';
import { WasNotFound } from '../modules/exceptions';
import { AdminGuard } from '../auth/auth.guard';
import { MongoOIDPipe } from '../modules/pipes';

@Controller('users')
@UseGuards(AdminGuard)
export class UsersController {
    constructor(private readonly service: UsersService) {}
    
    // Get all users
    @Get()
    getUsers(@Query('string') string?: string) {
        try {
            return this.service.getUsers(string)
        } catch (e) {
            throw e
        }    
    }

    // Get a single user
    @Get(':id')
    getUser(@Param('id', MongoOIDPipe) id: string) {
        try {
            return this.service.getUser(id)
        } catch (e) {
            throw e
        }        
    }

    // Create a new user
    @Post()
    newUser(@Body(new ValidationPipe()) body: NewUserSchema) {
        try {
            return this.service.newUser(body)  
        } catch (e) {
            throw e
        }        

    }

    // Update an existing user
    @Put(':id')
    updateUser(@Param('id', MongoOIDPipe) id: string, @Body() body: UpdateUserSchema) {
        try {
            return this.service.updateUser(id, body)
        } catch (e) {
            throw e
        }   
    }

    // Delete an existing user
    @Delete(':id')
    deleteUser(@Param('id', MongoOIDPipe) id: string) {
        try {
            return this.service.removeUser(id)
        } catch (e) {
            throw e
        }
    }
}
