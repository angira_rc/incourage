import { Model } from 'mongoose';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { hash } from 'bcryptjs'

import { User } from '../models/user';
import { NewUserSchema, UpdateUserSchema } from './dto';
import { parseUser } from '../modules/helpers';
import { AlreadyExists, WasNotFound } from '../modules/exceptions';

@Injectable()
export class UsersService {
    constructor(@InjectModel(User.name) private userModel: Model<User>) {}

    async getUsers(string?: string) {
        let ops: any = {}
        if (string)
            ops.name = new RegExp(string, 'i')

        let users: any[] = await this.userModel.find(ops).lean()
        users = users.map(parseUser)

        return users
    }

    async getUser(id: string) {
        let match = await this.userModel.findById(id).lean()
        if (!match)
            throw new WasNotFound('user')

        return parseUser(match)
    }

    async newUser(body: NewUserSchema) {
        const exists = await this.userModel.findOne({ email: body.email, username: body.username });
        if (exists)
            throw new AlreadyExists(`A user with the specified ${exists.username === body.username ? 'username' : 'email'}`, '')

        if (body.username === 'root')
            throw new HttpException('Cannot use the username root', HttpStatus.BAD_REQUEST)

        let password = await hash(body.password.toString(), 12)

        let user = new this.userModel({
            ...body,
            password
        })
        
        user = await user.save()

        return parseUser(user)
    }

    async updateUser(id: string, body: UpdateUserSchema) {
        let match = await this.userModel.findById(id)
        if (!match)
            throw new WasNotFound('user')

        if (match.username === 'root' && body.username !== 'root')
            throw new HttpException('Cannot update the root username', HttpStatus.BAD_REQUEST)

        if (body.username === 'root' && match.username !== 'root')
            throw new HttpException('Cannot use the username root', HttpStatus.BAD_REQUEST)

        const exists = await this.userModel.findOne({ email: body.email, username: body.username, _id: { $ne: match._id } });
        if (exists)
            throw new AlreadyExists(`A user with the specified ${exists.username === body.username ? 'username' : 'email'}`, '')

        match.name = body.name
        match.username = body.username
        match.email = body.email

        if (body.password)
            match.password = await hash(body.password.toString(), 12)

        match = await match.save()

        return parseUser(match)
    }

    async removeUser(id: string) {
        let match = await this.userModel.findById(id).lean()
        if (!match)
            throw new WasNotFound('User not found')

        if (match.username === 'root')
            throw new HttpException('Cannot delete root user', HttpStatus.BAD_REQUEST)

        await this.userModel.findByIdAndDelete(id)

        return parseUser(match)
    }
}
