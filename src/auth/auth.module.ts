import { JwtModule } from '@nestjs/jwt';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { UserSchema } from '../models/user';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { ParametersSchema } from '../models/system';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: 'User', schema: UserSchema },
            { name: 'Parameters', schema: ParametersSchema }
        ]),
        JwtModule.register({
            global: true,
            secret: process.env.JWT_SECRET,
            signOptions: { expiresIn: '10m' },
        })
    ],
    controllers: [AuthController],
    providers: [AuthService]
})
export class AuthModule { }
