import moment from 'moment';
import { Test } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { testBootstrap } from '../modules/bootstrap';

describe('AuthController', () => {
    let authController: AuthController;
    let authService: AuthService;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule(testBootstrap).compile();

        authService = moduleRef.get<AuthService>(AuthService);
        authController = moduleRef.get<AuthController>(AuthController);
    });

    describe('logIn', () => {
        it('should return a access token for the logged in user', async () => {
            const result = { access_token: 'abcdef' }

            jest.spyOn(authService, 'logIn').mockImplementation(() => Promise.resolve(result));
            let res = await authController.signIn({
                username: 'robin',
                password: 'password'
            })

            expect(res).toBe(result);
        });
    });

    describe('signUp', () => {
        it('should create a new agent', async () => {
            const result = {
                _id: "0",
                username: 'robin',
                name: 'Robin Angira',
                email: 'rangira@gmail.com',
                type: 'admin',
                date: new Date(),
            }

            jest.spyOn(authService, 'newAgent').mockImplementation(() => Promise.resolve(result));
            let res = await authController.signUp({
                username: 'robin',
                name: 'Robin Angira',
                email: 'rangira@gmail.com',
                password: 'password',
            })

            expect(res).toBe(result);
        });
    });
});