import { Model } from 'mongoose';
import { compare, hash } from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { HttpException, HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';

import { User } from '../models/user';
import { NewAgentSchema } from './dto';
import { Parameters } from '../models/system';
import { parseUser } from '../modules/helpers';
import { AlreadyExists } from '../modules/exceptions';

@Injectable()
export class AuthService {
    constructor(
        @InjectModel(User.name) private userModel: Model<User>,
        @InjectModel(Parameters.name) private paramsModel: Model<Parameters>,
        private jwtService: JwtService) {}

    async logIn(username: string, password: string): Promise<{ access_token: string }> {
        const match = await this.userModel.findOne({ username });
        if (!match)
            throw new UnauthorizedException()

        let valid = await compare((password ? password.toString() : ''), match.password)
        if (!valid)
            throw new UnauthorizedException()
        
        let access_token = await this.jwtService.signAsync({ id: match._id, username: match.username })

        return { access_token };
    }

    async newAgent(body: NewAgentSchema): Promise<any> {
        if (body.username === 'root')
            throw new HttpException('Cannot use the username root', HttpStatus.BAD_REQUEST)

        const match = await this.userModel.findOne({ email: body.email, username: body.username });
        if (match)
            throw new AlreadyExists('A user with the specified email or username', '')

        let password = await hash(body.password.toString(), 12)

        let user = new this.userModel({
            ...body,
            type: 'agent',
            password
        })
        
        user = await user.save()

        return parseUser(user)
    }

    async initialize(): Promise<any> {
        const match = await this.userModel.findOne({ username: "root" });
        if (match)
            throw new HttpException('The system has already been initialized', HttpStatus.BAD_REQUEST)

        let password = await hash("pass@inc0urage", 12)

        let user = new this.userModel({
            username: "root",
            name: "Root User",
            email: "root@incourage.com",
            type: 'admin',
            password
        })
        
        user = await user.save()

        let params = new this.paramsModel({ pcCommission: 5 })
        await params.save()

        return { status: "OK" }
    }
}