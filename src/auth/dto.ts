import { MinLength } from "class-validator";

export class LoginSchema { 
    @MinLength(3)
    username: string; 

    @MinLength(6)
    password: string;
}

export class NewAgentSchema {
    @MinLength(3)
    name: string;
    
    @MinLength(4)
    username: string;
    
    @MinLength(6)
    email: string;
    
    @MinLength(6)
    password: string;
}