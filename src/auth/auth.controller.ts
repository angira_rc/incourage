import { AuthService } from './auth.service';
import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { LoginSchema, NewAgentSchema } from './dto';

@Controller('auth')
export class AuthController {
    constructor(private readonly service: AuthService) {}

    @HttpCode(HttpStatus.OK)
    @Post('login')
    signIn(@Body() body: LoginSchema) {
        try {
            return this.service.logIn(body.username, body.password);
        } catch (e) {
            throw e
        }  
    }

    @HttpCode(HttpStatus.OK)
    @Post('sign-up')
    signUp(@Body() body: NewAgentSchema) {
        try {
            return this.service.newAgent(body);
        } catch (e) {
            throw e
        }  
    }

    @HttpCode(HttpStatus.OK)
    @Post('init')
    initialize() {
        try {
            return this.service.initialize();
        } catch (e) {
            throw e
        }  
    }
}
