import { Request } from '@nestjs/common';
import { Product } from 'src/models/product';
import { User } from 'src/models/user';

export interface ReqWithUser extends Request {
    user: any;
}

export interface Transaction {
    _id: string;
    agent: User;
    product: Product;
    amount: number;
    quantity: number;
    ledger: string;
    date: Date;
}