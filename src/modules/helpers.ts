import * as fs from 'fs'
import Handlebars from 'handlebars'
import { User } from "../models/user"

export const parseUser = (user: any) => {
    let usr = user?._doc || user
    delete usr.password
    
    return usr
}

export const stringifyDate = (date, noTime = false) => {
    let d = new Date(date).toString()
    d = d.slice(0, d.indexOf(' GMT'))

    if (noTime)
        d = d.slice(0, d.lastIndexOf(' '))

    return d
}

export const hbsHelpers = () => {
    Handlebars.registerPartial('styles', Handlebars.compile(fs.readFileSync("./src/pdf/templates/partials/styles.hbs", "utf8")))
    Handlebars.registerHelper("inc", value => parseInt(value) + 1)
    Handlebars.registerHelper("mult", (a, b) => a * b)
    Handlebars.registerHelper("sum", (a, b) => a + b)
    Handlebars.registerHelper("diff", (a, b) => a - b)
    Handlebars.registerHelper("join", value => value.join(', '))
    Handlebars.registerHelper("eq", (a, b) => a === b)
    Handlebars.registerHelper("neq", (a, b) => a !== b)
}