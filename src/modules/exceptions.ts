import { HttpException } from "@nestjs/common";

export class WasNotFound extends HttpException {
    constructor(model: string, prefix: string = 'The specified') {
        super(`${prefix} ${model} wasn't found`, 404);
    }
}

export class WasNotFoundMult extends HttpException {
    constructor(model: string, prefix: string = 'One or more of the specified') {
        super(`${prefix} ${model} wasn't found`, 404);
    }
}

export class AlreadyExists extends HttpException {
    constructor(model: string, prefix: string = 'The specified') {
        super(`${prefix} ${model} already exists`, 400);
    }
}