import { PipeTransform, ArgumentMetadata, BadRequestException } from '@nestjs/common';
import { isValidObjectId } from 'mongoose';

export class MongoOIDPipe implements PipeTransform {
    transform(value: unknown) {
        if (!isValidObjectId(value))
            throw new BadRequestException('Wrong id format')
        
        return value
    }
}

export class ParseDatePipe implements PipeTransform {
    transform(value: string) {           
        return new Date(value)
    }
}
