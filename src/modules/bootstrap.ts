import 'dotenv/config'
import { MongooseModule } from '@nestjs/mongoose'

import { AppService } from '../app.service'
import { PdfModule } from '../pdf/pdf.module'
import { PdfService } from '../pdf/pdf.service'
import { AuthModule } from '../auth/auth.module'
import { AppController } from '../app.controller'
import { EmailModule } from '../email/email.module'
import { UsersModule } from '../users/users.module'
import { LegderModule } from '../legder/legder.module'
import { ProductsModule } from '../products/products.module'
import { LegderSchema, UserSchema } from "../models/user"
import { TransactionsModule } from '../transactions/transactions.module'

export const testBootstrap = {
    imports: [
        UsersModule,
        ProductsModule,
        TransactionsModule,
        MongooseModule.forFeature([
            { name: 'Legder', schema: LegderSchema },
            { name: 'User', schema: UserSchema }
        ]),
        MongooseModule.forRoot(process.env.MONGODB_URI),
        LegderModule,
        AuthModule,
        EmailModule,
        PdfModule
    ],
    controllers: [AppController],
    providers: [AppService, PdfService],
}