import { Module } from '@nestjs/common';
import { TransactionsController } from './transactions.controller';
import { TransactionsService } from './transactions.service';
import { TransactionSchema } from '../models/transaction';
import { MongooseModule } from '@nestjs/mongoose';
import { ParametersSchema } from '../models/system';
import { LegderSchema, UserSchema } from '../models/user';
import { ProductSchema } from '../models/product';

@Module({
  imports: [MongooseModule.forFeature([
    { name: 'Transaction', schema: TransactionSchema }, 
    { name: 'Parameters', schema: ParametersSchema }, 
    { name: 'User', schema: UserSchema }, 
    { name: 'Product', schema: ProductSchema },
    { name: 'Legder', schema: LegderSchema }
  ])],
  controllers: [TransactionsController],
  providers: [TransactionsService]
})
export class TransactionsModule {}
