import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { NewTransactionSchema } from './dto';

import { Transaction as TransactionPopulated } from '../modules/interfaces';
import { Product } from '../models/product';
import { Transaction } from '../models/transaction';
import { User, Legder } from '../models/user';
import { Parameters } from '../models/system';
import { WasNotFound } from '../modules/exceptions';

@Injectable()
export class TransactionsService {
    constructor(
        @InjectModel(Transaction.name) private transactionModel: Model<Transaction>,
        @InjectModel(Parameters.name) private paramsModel: Model<Parameters>,
        @InjectModel(Product.name) private productModel: Model<Product>,
        @InjectModel(Legder.name) private legderModel: Model<Legder>) {}

    async getTransactions(from: Date, to: Date, product?: string, user?: string): Promise<Transaction[]> {
        let ops: any = { agent: user }

        if (product)
            ops.product = product

        if (from || to) {
            ops.date = {}
            if (from)
                ops.date.$gte = from

            if (to)
                ops.date.$lte = to
        }

        let transactions = await this.transactionModel.find(ops)
            .populate('agent')
            .populate('product')
            .lean()
            
        return transactions
    }

    async getTransaction(id: string): Promise<Transaction> {
        let match = await this.transactionModel.findById(id)
            .populate('agent')
            .populate('product')
            .lean()
        if (!match)
            throw new WasNotFound('transaction')

        return match
    }

    async newTransaction(body: NewTransactionSchema, user: User): Promise<Transaction> {
        // TODO: Fix params not found error
        let params = await this.paramsModel.findOne({})
        if (!params)
            throw new WasNotFound('Parameters', '')

        let product = await this.productModel.findById(body.product)
        if (!product)
            throw new WasNotFound('product')

        let commission = Number(body.amount) * (params.pcCommission / 100)

        let latest = await this.legderModel.find({ user: user._id }).sort({ date: -1 }).limit(1)
        let balance = latest.length > 0 ? latest[0].balance : 0

        let legder = new this.legderModel({
            user: user._id,
            description: `Commission earned from sale of ${ body.quantity } ${ body.quantity > 1 ? 'units' : 'unit' } of ${ product.name }`,
            credit: commission,
            debit: 0,
            value: body.amount * body.quantity,
            balance: balance + commission
        })

        legder = await legder.save()

        let transaction = new this.transactionModel({ agent: user._id, commission, ...body, legder: legder.id })
        transaction = await transaction.save()
        
        return transaction
    }
}
