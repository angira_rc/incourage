import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards, ValidationPipe, Request } from '@nestjs/common';
import { NewTransactionSchema } from './dto';
import { TransactionsService } from './transactions.service';
import { WasNotFound } from '../modules/exceptions';
import { MongoOIDPipe, ParseDatePipe } from '../modules/pipes';
import { AdminGuard, AuthGuard } from '../auth/auth.guard';
import { ReqWithUser } from '../modules/interfaces';

@Controller('transactions')
@UseGuards(AuthGuard)
export class TransactionsController {
    constructor(private readonly service: TransactionsService) {}

    // Get all transactions
    @Get()
    @UseGuards(AdminGuard)
    getTransactions(@Query('product') product?: string, user?: string, @Query('from', ParseDatePipe) from?: Date, @Query('to', ParseDatePipe) to?: Date) {
        try {
            return this.service.getTransactions(from, to, product, user)
        } catch (e) {
            throw e
        }  
    }

    // Get transactions by logged in user
    @Get('mine')
    getMyTransactions(@Query('product') product?: string, @Request() request?: ReqWithUser, @Query('from', ParseDatePipe) from?: Date, @Query('to', ParseDatePipe) to?: Date) {
        try {
            return this.service.getTransactions(from, to, product, request.user._id)
        } catch (e) {
            throw e
        }  
    }

    // Get a single transaction
    @UseGuards(AdminGuard)
    @Get(':id')
    getTransaction(@Param('id', MongoOIDPipe) id: string) {
        try {
            return this.service.getTransaction(id)
        } catch (e) {
            throw new WasNotFound('transaction')
        }        
    }

    // Create a new transaction
    @Post()
    newTransaction(@Body(new ValidationPipe()) body: NewTransactionSchema, @Request() request?: ReqWithUser) {
        try {
            return this.service.newTransaction(body, request.user)        
        } catch (e) {
            throw e
        }  
    }
}
