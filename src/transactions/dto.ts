export class NewTransactionSchema {
    product: string;
    amount: number;
    quantity: number;
}