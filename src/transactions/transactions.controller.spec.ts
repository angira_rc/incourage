import { MongooseModule } from '@nestjs/mongoose';

import { Test } from '@nestjs/testing';
import { TransactionsService } from './transactions.service';
import { TransactionsController } from './transactions.controller';
import { testBootstrap } from '../modules/bootstrap';

describe('TransactionsController', () => {
    let transactionsController: TransactionsController;
    let transactionsService: TransactionsService;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule(testBootstrap).compile();

        transactionsService = moduleRef.get<TransactionsService>(TransactionsService);
        transactionsController = moduleRef.get<TransactionsController>(TransactionsController);
    });

    describe('getTransactions', () => {
        it('should return an array of transactions', async () => {
            const result = [
                {
                    _id: "1",
                    agent: "2",
                    product: "1",
                    amount: 205000,
                    quantity: 1,
                    legder: "564",
                    date: new Date()
                }
            ]

            jest.spyOn(transactionsService, 'getTransactions').mockImplementation(() => Promise.resolve(result));
            let res = await transactionsController.getTransactions()

            expect(res).toBe(result);
        });
    });

    describe('getTransaction', () => {
        it('should return a single user', async () => {
            const result = {
                _id: "1",
                agent: "2",
                product: "1",
                amount: 205000,
                quantity: 1,
                legder: "564",
                date: new Date()
            }

            jest.spyOn(transactionsService, 'getTransaction').mockImplementation(() => Promise.resolve(result));
            let res = await transactionsController.getTransaction(result._id)

            expect(res).toBe(result);
        });
    });

    describe('newTransaction', () => {
        it('should record a new transaction', async () => {
            const result = {
                _id: "1",
                agent: "2",
                product: "1",
                amount: 205000,
                quantity: 1,
                legder: "564",
                date: new Date()
            }

            jest.spyOn(transactionsService, 'newTransaction').mockImplementation(() => Promise.resolve(result));
            let res = await transactionsController.newTransaction({
                product: "012",
                amount: 2000,
                quantity: 2
            })

            expect(res).toBe(result);
        });
    });
});