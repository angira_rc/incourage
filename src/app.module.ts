import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { AppController } from './app.controller';
import { UsersModule } from './users/users.module';
import { LegderModule } from './legder/legder.module';
import { ProductsModule } from './products/products.module';
import { TransactionsModule } from './transactions/transactions.module';
import { EmailModule } from './email/email.module';
import { PdfModule } from './pdf/pdf.module';
import { LegderSchema, UserSchema } from './models/user';
import { PdfService } from './pdf/pdf.service';

@Module({
    imports: [
        UsersModule,
        ProductsModule,
        TransactionsModule,
        MongooseModule.forFeature([
            { name: 'Legder', schema: LegderSchema },
            { name: 'User', schema: UserSchema }
        ]),
        MongooseModule.forRoot(process.env.MONGODB_URI),
        LegderModule,
        AuthModule,
        EmailModule,
        PdfModule
    ],
    controllers: [AppController],
    providers: [AppService, PdfService],
})
export class AppModule { }
