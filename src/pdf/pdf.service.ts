import * as fs from 'fs'
import { v4 } from 'uuid'
import * as pdf from 'pdf-creator-node'
import { Injectable } from '@nestjs/common';

import { hbsHelpers } from '../modules/helpers';

@Injectable()
export class PdfService {
    template: string
    options = {
        format: "A4",
        orientation: "portrait",
        border: "10mm"
    }
    
    constructor() { 
        hbsHelpers()
        this.template = fs.readFileSync("src/pdf/templates/mail.hbs", "utf8")
    }


    async generate(data: any): Promise<string[]> {
        let name = `${v4()}.pdf`
        let path = `src/pdfs/${name}`
    
        var document = {
            html: this.template,
            data,
            path,
            type: "",
        }        

        await pdf.create(document, this.options)
        // const file = await fs.promises.readFile(path, "binary")

        return [name, path]
    }

    async clearTemp(path: string): Promise<void> {
        await fs.promises.unlink(path)
    }
}
