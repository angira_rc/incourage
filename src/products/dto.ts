import { PartialType } from "@nestjs/mapped-types";
import { MinLength } from "class-validator";

export class NewProductSchema {
    @MinLength(3)
    name: string;
    price: number;
    category: string;
    // createdOn: string;
}

export class UpdateProductSchema extends PartialType(NewProductSchema) {}