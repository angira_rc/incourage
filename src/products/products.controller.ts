import { Body, Controller, Delete, Get, Param, Post, Put, Query, ValidationPipe, UseGuards } from '@nestjs/common';
import { NewProductSchema, UpdateProductSchema } from './dto';
import { ProductsService } from './products.service';
import { WasNotFound } from '../modules/exceptions';
import { AdminGuard, AuthGuard } from '../auth/auth.guard';
import { MongoOIDPipe } from '../modules/pipes';

@Controller('products')
export class ProductsController {
    constructor(private readonly service: ProductsService) {}

    // Get multiple products
    @Get()
    @UseGuards(AuthGuard)
    getProducts(@Query('category') category?: string, @Query('string') string?: string) {
        try {
            return this.service.getProducts(category, string)
        } catch (e) {
            throw e
        }  
    }

    // Get a single product
    @Get(':id')
    @UseGuards(AuthGuard)
    getProduct(@Param('id', MongoOIDPipe) id: string) {
        try {
            return this.service.getProduct(id)
        } catch (e) {
            throw new WasNotFound('product')
        }        
    }

    // Create a new product
    @Post()
    @UseGuards(AdminGuard)
    newProduct(@Body(new ValidationPipe()) body: NewProductSchema) {
        try {
            return this.service.newProduct(body)        
        } catch (e) {
            throw e
        }  
    }

    // Update an existing product
    @Put(':id')
    @UseGuards(AdminGuard)
    updateProduct(@Param('id', MongoOIDPipe) id: string, @Body() body: UpdateProductSchema) {
        try {
            return this.service.updateProduct(id, body)
        } catch (e) {
            throw e
        }   
    }

    // Delete an existing product
    @Delete(':id')
    @UseGuards(AdminGuard)
    deleteProduct(@Param('id', MongoOIDPipe) id: string) {
        try {
            return this.service.removeProduct(id)
        } catch (e) {
            throw e
        }
    }
}
