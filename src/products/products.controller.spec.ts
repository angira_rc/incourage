import { MongooseModule } from '@nestjs/mongoose';

import { Test } from '@nestjs/testing';
import { UserSchema } from '../models/user';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { testBootstrap } from '../modules/bootstrap';

describe('ProductsController', () => {
    let productsController: ProductsController;
    let productsService: ProductsService;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule(testBootstrap).compile();

        productsService = moduleRef.get<ProductsService>(ProductsService);
        productsController = moduleRef.get<ProductsController>(ProductsController);
    });

    describe('getProducts', () => {
        it('should return an array of products', async () => {
            const result = [
                {
                    _id: "0",
                    name: "IPhone 15 Plus",
                    category: "Mobile Phones",
                    price: 190000,
                    date: new Date()
                }
            ]

            jest.spyOn(productsService, 'getProducts').mockImplementation(() => Promise.resolve(result));
            let res = await productsController.getProducts()

            expect(res).toBe(result);
        });

        it('should return an array of products matching "test"', async () => {
            const result = [
                {
                    _id: "0",
                    name: "IPhone 15 Plus",
                    category: "Mobile Phones",
                    price: 190000,
                    date: new Date()
                }
            ]

            jest.spyOn(productsService, 'getProducts').mockImplementation(() => Promise.resolve(result));
            let res = await productsController.getProducts('test')

            expect(res).toBe(result);
        });
    });

    describe('getProduct', () => {
        it('should return a single user', async () => {
            const result = {
                _id: "0",
                name: "IPhone 15 Plus",
                category: "Mobile Phones",
                price: 190000,
                date: new Date()
            }

            jest.spyOn(productsService, 'getProduct').mockImplementation(() => Promise.resolve(result));
            let res = await productsController.getProduct(result._id)

            expect(res).toBe(result);
        });
    });

    describe('newProduct', () => {
        it('should add a new product', async () => {
            const result = {
                _id: "0",
                name: "IPhone 15 Plus",
                category: "Mobile Phones",
                price: 190000,
                date: new Date()
            }

            jest.spyOn(productsService, 'newProduct').mockImplementation(() => Promise.resolve(result));
            let res = await productsController.newProduct({
                name: "IPhone 15 Plus",
                category: "Mobile Phones",
                price: 190000
            })

            expect(res).toBe(result);
        });
    });

    describe('updateUser', () => {
        it('should update the product\'s details', async () => {
            const result = {
                _id: "0",
                name: "IPhone 15 Plus",
                category: "Mobile Phones",
                price: 190000,
                date: new Date()
            }

            jest.spyOn(productsService, 'updateProduct').mockImplementation(() => Promise.resolve(result));
            let res = await productsController.updateProduct("0", {
                name: "IPhone 15 Plus",
                category: "Mobile Phones",
                price: 190000
            })

            expect(res).toBe(result);
        });
    });

    describe('deleteProduct', () => {
        it('should delete new user', async () => {
            const result = {
                _id: "0",
                name: "IPhone 15 Plus",
                category: "Mobile Phones",
                price: 190000,
                date: new Date()
            }

            jest.spyOn(productsService, 'removeProduct').mockImplementation(() => Promise.resolve(result));
            let res = await productsController.deleteProduct("0")

            expect(res).toBe(result);
        });
    });
});