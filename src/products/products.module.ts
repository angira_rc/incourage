import { Module } from '@nestjs/common';
import { ProductsController } from './products.controller';
import { ProductsService } from './products.service';
import { ProductSchema } from '../models/product';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from '../models/user';

@Module({
  imports: [MongooseModule.forFeature([
    { name: 'User', schema: UserSchema }, 
    { name: 'Product', schema: ProductSchema }
  ])],
  controllers: [ProductsController],
  providers: [ProductsService]
})
export class ProductsModule {}
