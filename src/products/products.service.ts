import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { Product } from '../models/product';
import { NewProductSchema, UpdateProductSchema } from './dto';
import { WasNotFound } from 'src/modules/exceptions';

@Injectable()
export class ProductsService {
    constructor(@InjectModel(Product.name) private productModel: Model<Product>) {}

    async getProducts(category?: string, string?: string): Promise<Product[]> {
        let ops: any = {}

        if (category)
            ops.category = category

        if (string)
            ops.string = new RegExp(string, 'i')

        let products = await this.productModel.find(ops).lean()
            
        return products
    }

    async getProduct(id: string): Promise<Product> {
        let match = await this.productModel.findById(id).lean()
        if (!match)
            throw new WasNotFound('product')

        return match
    }

    async newProduct(body: NewProductSchema): Promise<Product> {
        let product = new this.productModel(body)
        
        product = await product.save()
        
        return product
    }

    async updateProduct(id: string, body: UpdateProductSchema): Promise<Product> {
        let match = await this.productModel.findById(id)
        if (!match)
            throw new WasNotFound('product')

        match.name = body.name
        match.category = body.category
        match.price = body.price

        match = await match.save()

        return match
    }

    async removeProduct(id: string): Promise<Product> {
        let match = await this.productModel.findById(id).lean()
        if (!match)
            throw new WasNotFound('product')

        await this.productModel.findByIdAndDelete(id)

        return match
    }
}
