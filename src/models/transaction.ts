import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Schema as Schma } from 'mongoose';

@Schema()
export class Transaction {
    @Prop({ type: Schma.Types.ObjectId, ref: 'User', required: true })
    agent: string;

    @Prop({ type: Schma.Types.ObjectId, ref: 'Product', required: true })
    product: string;

    @Prop({ required: true })
    amount: number;

    @Prop({ required: true })
    quantity: number;

    @Prop({ type: Schma.Types.ObjectId, ref: 'Product', required: true })
    legder: string;

    @Prop({ default: Date.now })
    date: Date;
}

export const TransactionSchema = SchemaFactory.createForClass(Transaction);