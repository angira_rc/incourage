import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Schema as Schma } from 'mongoose';

@Schema()
export class User {
  _id: string;

  @Prop({ required: true })
  name: string;
  
  @Prop({ required: true })
  username: string;
  
  @Prop({ required: true })
  email: string;
  
  @Prop({ required: true })
  password: string;
  
  @Prop({ required: true })
  type: string;

  @Prop({ default: Date.now, required: true })
  date: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);

@Schema()
export class Legder {
  @Prop({ type: Schma.Types.ObjectId, ref: 'User', required: true })
  user: string;
  
  @Prop({ required: true })
  description: string;

  @Prop({ required: true })
  debit: number;

  @Prop({ required: true })
  credit: number;

  @Prop({ required: true })
  balance: number;

  @Prop({ required: true })
  value: number;

  @Prop({ default: Date.now })
  date: Date;
}

export const LegderSchema = SchemaFactory.createForClass(Legder);