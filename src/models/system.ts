import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class Parameters {
    @Prop({ required: true })
    pcCommission: number;
}

export const ParametersSchema = SchemaFactory.createForClass(Parameters);