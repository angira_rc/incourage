import { Controller, Get, Request, Query, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { ReqWithUser } from './modules/interfaces';
import { EmailService } from './email/email.service';
import { ParseDatePipe } from './modules/pipes';
import { stringifyDate } from './modules/helpers';
import { InjectModel } from '@nestjs/mongoose';
import { Legder } from './models/user';
import { Model } from 'mongoose';
import { AuthGuard } from './auth/auth.guard';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) { }

    @Get()
    getHello(): string {
        return this.appService.getHello();
    }

    // Send Email to Agents
    @Get('getStatement')
    @UseGuards(AuthGuard)
    async sendStatement(@Query('from', ParseDatePipe) from: Date, @Query('to', ParseDatePipe) to: Date, @Request() request: ReqWithUser) {
        try {
            return this.appService.sendStatement(from, to, request.user)
        } catch (e) {
            throw e
        }

    }
}
