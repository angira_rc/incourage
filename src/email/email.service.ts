import { Injectable } from '@nestjs/common';
import { MailClient } from './mail-client';
import { EmailSchema } from './dto';

@Injectable()
export class EmailService {
    constructor(private readonly client: MailClient) { }

    async sendEmail(mail: EmailSchema): Promise<void> {
        await this.client.send(mail);
    }
}