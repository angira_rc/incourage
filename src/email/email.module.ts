// src/email/email.module.ts

import { Module } from '@nestjs/common';
import { EmailService } from './email.service';
import { MailClient } from './mail-client';

@Module({
  providers: [EmailService, MailClient],
  exports: [EmailService],
})

export class EmailModule {}