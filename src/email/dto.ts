export class EmailAttachment {
    filename: string;
    path: any;
}

export class EmailSchema {
    subject: string
    to: string;
    content: string;
    attachments?: EmailAttachment[]
}
