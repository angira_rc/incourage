import { Injectable, Logger } from '@nestjs/common';
import * as nodemailer from 'nodemailer'

import { EmailSchema } from './dto'

const { COMPANY_NAME, EMAIL_ADDRESS, EMAIL_PASSWORD, EMAIL_HOST, EMAIL_PORT } = process.env

@Injectable()
export class MailClient {
    private logger: Logger;
    private transporter: any;

    constructor() {
        this.logger = new Logger("Mail")
        this.transporter = nodemailer.createTransport({
            host: EMAIL_HOST,
            port: EMAIL_PORT,
            auth: {
              user: EMAIL_ADDRESS,
              pass: EMAIL_PASSWORD
            }
        });
    }
    
    async send(mail: EmailSchema): Promise<void> {
        try {
            await this.transporter.sendMail({
                from: `${COMPANY_NAME} <${EMAIL_ADDRESS}>`,
                to: mail.to,
                subject: mail.subject,
                html: mail.content,
                attachments: mail.attachments
            })

            // await fetch('https://api.brevo.com/v3/smtp/email', {
            //     method: 'POST',
            //     headers: {
            //         'api-key': process.env.BREVO_API_KEY,
            //         'Content-Type': 'application/json'
            //     },
            //     body: JSON.stringify({  
            //         sender: mail.from,
            //         to: mail.to,
            //         subject: mail.subject,
            //         htmlContent: mail.content
            //     })
            // })
            this.logger.log(`Email successfully dispatched to ${mail.to}`);
        } catch (error) {
            console.log(error)
            //You can do more with the error
            this.logger.error('Error while sending email', error);
            throw error;
        }
    }
}